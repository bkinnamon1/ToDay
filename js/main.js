const listRoot = document.querySelector('.item-list');
const hideControl = document.querySelector('#hide-completed');
const moveControl = document.querySelector('#move-completed');
const newItemForm = document.querySelector('#new-item-form');
const itemTemplate = document.querySelector('#item');
const editItemTemplate = document.querySelector('#edit-item');

let shouldHideCompleted = false;
let shouldMoveCompleted = false;
let items = [];

hideControl.addEventListener(
  'change',
  () => {
    shouldHideCompleted = !shouldHideCompleted;
    updateItems();
  },
  false
);
moveControl.addEventListener(
  'change',
  () => {
    shouldMoveCompleted = !shouldMoveCompleted;
    updateItems();
  },
  false
);

newItemForm.addEventListener(
  'submit',
  e => {
    e.preventDefault();
    let nameInput = e.target.querySelector('input[type=text]');
    const name = nameInput.value;
    addItem(name);
    nameInput.value = '';
  },
  false
);

function updateItems() {
  listRoot.innerHTML = '';

  if (shouldMoveCompleted) {
    undoneItems = items.filter(item => !item.isComplete);
    doneItems = items.filter(item => item.isComplete);

    listItems(undoneItems);
    listItems(doneItems);
  } else {
    listItems(items);
  }
}

function listItems(itemList) {
  itemList.forEach(item => {
    if (item.isComplete && shouldHideCompleted) return;

    if (item.isEditing) {
      insertEditItem(item);
    } else {
      insertItem(item);
    }
  });
}

function insertEditItem(item) {
  let element = document.importNode(editItemTemplate.content, true);
  inputElement = element.querySelector('input[name=name]');
  cancelElement = element.querySelector('.cancel-edit');

  inputElement.value = item.name;
  inputElement.addEventListener(
    'blur',
    () => updateItem(item, inputElement.value),
    false
  );
  inputElement.addEventListener(
    'keydown',
    e => {
      switch (e.key) {
        case 'Enter':
          updateItem(item, inputElement.value);
          break;
        case 'Escape':
          inputElement.value = item.name;
          cancelEditItem(item);
          break;
      }
    },
    false
  );
  cancelElement.addEventListener(
    'mousedown',
    () => {
      inputElement.value = item.name;
      cancelEditItem(item);
    },
    false
  );

  listRoot.appendChild(element);
  inputElement.focus();
}

function insertItem(item) {
  let element = document.importNode(itemTemplate.content, true);
  itemElement = element.querySelector('.item');
  nameElement = element.querySelector('.item-name');
  editElement = element.querySelector('.item-edit');
  deleteElement = element.querySelector('.item-delete');

  itemElement.addEventListener('click', () => toggleItem(item), false);
  nameElement.innerText = item.name;
  editElement.addEventListener(
    'click',
    e => {
      e.stopPropagation();
      editItem(item);
    },
    false
  );
  deleteElement.addEventListener('click', () => deleteItem(item), false);

  if (item.isComplete) {
    nameElement.classList.add('complete');
  }

  listRoot.appendChild(element);
}

function addItem(name) {
  item = { name, isComplete: false, isEditing: false };
  items.unshift(item);
  updateItems();
}

function toggleItem(item) {
  item.isComplete = !item.isComplete;
  updateItems();
}

function updateItem(item, newName) {
  item.name = newName;
  item.isEditing = false;
  updateItems();
}

function editItem(item) {
  item.isEditing = true;
  updateItems();
}

function cancelEditItem(item) {
  item.isEditing = false;
  updateItems();
}

function deleteItem(item) {
  const index = items.indexOf(item);
  items.splice(index, 1);
  updateItems();
}

updateItems();
